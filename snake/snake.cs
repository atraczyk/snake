﻿//-----------------------------------------------------------------------------
//	console snake game by Andreas Traczyk (2014-15) GPL
//	http://atraczyk.co.nf/	email: andreastraczyk@gmail.com
//
//	
//-----------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace snake
{
    enum DIRECTION { Left, Right, Up, Down };

    class snake
    {
        static void Main(string[] args)
        {
            ConsoleKey key = ConsoleKey.NoName;
            ConPlayer player = new ConPlayer(40, 20, 10);
            DateTime time = DateTime.Now;
            long tLast = time.Ticks;
            long tElapsed = 0;
            long FPS = 8;
            string timeformat = "h:mm:ss.fff";

            Console.Title = "SNAKE";
            Console.SetBufferSize(80, 40);
            Console.CursorVisible = false;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetWindowSize(80, 40);
            Console.OutputEncoding = System.Text.Encoding.GetEncoding(1252);

            Console.Clear();
            player.DrawBorder();
            player.AddNibblet();

            if (System.IO.File.Exists(@"hiscore.txt"))
                player.highscore = Convert.ToInt32(System.IO.File.ReadAllText(@"hiscore.txt"));
            else
                player.highscore = 0;

            while (true)
            {
                while (!Console.KeyAvailable)
                {
                    time = DateTime.Now;

                    tElapsed += (time.Ticks - tLast);
                    tLast = time.Ticks;

                    //update
                    if (tElapsed >= ((long)(1000 / (FPS + ((int)(player.score / 5000))))) 
                        * TimeSpan.TicksPerMillisecond)
                    {
                        tElapsed = 0;

                        Console.SetCursorPosition(0, 0);
                        Console.WriteLine(time.ToString(timeformat));

                        Console.SetCursorPosition(38, 0);
                        Console.Write("SNAKE       "); Console.Write("FPS: "); 
                        Console.Write((FPS + ((int)(player.score / 5000))));
                        Console.Write("   SCORE   "); Console.Write(player.score);
                        Console.SetCursorPosition(33, 39);
                        Console.Write("HIGH SCORE:  "); Console.Write(player.highscore);

                        //move player
                        player.moved = true;
                        switch (player.direction)
                        {
                            case DIRECTION.Left:
                                player.Move(-1, 0);
                                break;
                            case DIRECTION.Right:
                                player.Move(1, 0);
                                break;
                            case DIRECTION.Up:
                                player.Move(0, -1);
                                break;
                            case DIRECTION.Down:
                                player.Move(0, 1);
                                break;
                        }
                    }
                }

                key = Console.ReadKey(true).Key;
                switch (key)
                {
                    case ConsoleKey.Q:
                    case ConsoleKey.Escape:
                        if (player.score > player.highscore)
                        {
                            System.IO.File.WriteAllText(@"hiscore.txt", player.score.ToString());
                            player.highscore = player.score;
                        }
                        Console.Clear();
                        return;
                    case ConsoleKey.W:
                    case ConsoleKey.UpArrow:
                        if (player.direction != DIRECTION.Down && player.moved)
                        {
                            player.direction = DIRECTION.Up;
                            player.moved = false;
                        }
                        break;
                    case ConsoleKey.S:
                    case ConsoleKey.DownArrow:
                        if (player.direction != DIRECTION.Up && player.moved)
                        {
                            player.direction = DIRECTION.Down;
                            player.moved = false;
                        }
                        break;
                    case ConsoleKey.A:
                    case ConsoleKey.LeftArrow:
                        if (player.direction != DIRECTION.Right && player.moved)
                        {
                            player.direction = DIRECTION.Left;
                            player.moved = false;
                        }
                        break;
                    case ConsoleKey.D:
                    case ConsoleKey.RightArrow:
                        if (player.direction != DIRECTION.Left && player.moved)
                        {
                            player.direction = DIRECTION.Right;
                            player.moved = false;
                        }
                        break;
                }
            }
        }
    }

    class ConPlayer
    {
        public int x;
        public int y;
        public int score;
        public bool moved = true;
        List<int> bX = new List<int>();
        List<int> bY = new List<int>();
        public DIRECTION direction = DIRECTION.Up;
        public int length = 0;
        int[,] world = new int[80, 40];
        Random rnd = new Random();
        public int highscore = 0;

        public ConPlayer(int X, int Y, int bodyLength)
        {
            score = 0;
            length = bodyLength;
            x = X; y = Y;
            for (int i = 0; i < length; i++)
            {
                bX.Add(x);
                bY.Add(y + i + 1);
            }

            for (int cy = 0; cy < 40; cy++)
                for (int cx = 0; cx < 80; cx++)
                    world[cx, cy] = 0;
        }

        public void DrawBorder()
        {
            for (int c = 0; c < Console.BufferWidth; c++)
            {
                Console.SetCursorPosition(c, 2);
                Console.Write((char)219);
                Console.SetCursorPosition(c, Console.BufferHeight - 1);
                Console.Write((char)219);
            }
            for (int c = 2; c < Console.BufferHeight - 2; c++)
            {
                Console.SetCursorPosition(0, c);
                Console.Write((char)219);
                Console.SetCursorPosition(Console.BufferWidth - 1, c);
                Console.Write((char)219);
            }
        }

        public void Restart(int X, int Y, int bodyLength)
        {
            bX.Clear();
            bY.Clear();
            score = 0;
            length = bodyLength;
            x = X; y = Y;
            for (int i = 0; i < length; i++)
            {
                bX.Add(x);
                bY.Add(y + i + 1);
            }

            for (int cy = 0; cy < 40; cy++)
                for (int cx = 0; cx < 80; cx++)
                    world[cx, cy] = 0;
        }

        public void Grow()
        {
            length++;
            bX.Insert(0, x);
            bY.Insert(0, y);
        }

        public void AddNibblet()
        {
            int rx, ry;
            do
            {
                rx = rnd.Next(2, 78);
                ry = rnd.Next(3, 38);
            } while (world[rx, ry] == 1);
            world[rx, ry] = 2;
            Console.SetCursorPosition(rx, ry);
            Console.Write("*");
        }

        public void Move(int dx, int dy)
        {
            int tx = x, ty = y;
            x += dx; y += dy;
            switch (world[x, y])
            {
                case 0:
                    break;
                case 1:
                    Console.Write((char)0x07);
                    if (score > highscore)
                    {
                        System.IO.File.WriteAllText(@"hiscore.txt", score.ToString());
                        highscore = score;
                    }
                    Restart(40, 20, 2);
                    Console.Clear();
                    DrawBorder();
                    AddNibblet();
                    break;
                case 2:
                    Console.Write((char)0x07);
                    score += 1000;
                    for (int c = 0; c < 20; c++)
                        Grow();
                    AddNibblet();
                    break;
            }
            if (x < 1)
                x = Console.BufferWidth - 2;
            if (x > Console.BufferWidth - 2)
                x = 1;
            if (y < 2)
                y = Console.BufferHeight - 3;
            if (y > Console.BufferHeight - 3)
                y = 2;

            world[bX[length - 1], bY[length - 1]] = 0;
            Console.SetCursorPosition(bX[length - 1], bY[length - 1]);
            Console.Write(" ");

            for (int i = length - 1; i > 0; i--)
            {
                bX[i] = bX[i - 1];
                bY[i] = bY[i - 1];
            }
            bX[0] = tx;
            bY[0] = ty;

            world[x, y] = 1;
            Console.SetCursorPosition(x, y);
            switch (direction)
            {
                case DIRECTION.Up:
                    Console.Write((char)30);
                    break;
                case DIRECTION.Down:
                    Console.Write((char)31);
                    break;
                case DIRECTION.Left:
                    Console.Write((char)17);
                    break;
                case DIRECTION.Right:
                    Console.Write((char)16);
                    break;
            }
        }
    }
}

